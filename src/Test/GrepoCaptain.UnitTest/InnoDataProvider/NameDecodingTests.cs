using FluentAssertions;
using GrepoCaptain.InnoDataProvider.Utils;
using Xunit;

namespace GrepoCaptain.UnitTest.InnoDataProvider;

public class NameDecodingTests
{
    [Theory]
    [InlineData("kunumario", "kunumario")] // only lowercase
    [InlineData("OnSHeRoo", "OnSHeRoo")] // contains uppercase
    [InlineData("dan+a", "dan a")] // space
    [InlineData("boy.", "boy.")] // dot
    [InlineData("%2ABlackShadow%2A", "*BlackShadow*")] // star
    [InlineData("Nacs%C3%A1", "Nacsá")] // hungarian "á"
    [InlineData("S%C3%B6t%C3%A9t+Hercegn%C5%91", "Sötét Hercegnő")] // hungarian "ő"
    [InlineData("Ath%C3%A9n-m%C3%A1jus", "Athén-május")] // hyphen
    [InlineData("%3DZ%C3%BAZLak%3D", "=ZúZLak=")] // equal sign
    public void ConversionWorks(string encoded, string expected)
    {
        // Act
        string result = StringUtils.DecodeString(encoded);

        // Assert
        result.Should().Be(expected);
    }
}
