﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrepoCaptain.Common;

namespace GrepoCaptain.Tools.Utils;

internal static class ClipboardHelper
{
    internal static string GetClipboard()
    {
        return ThreadingHelper.RunInSTASync(Clipboard.GetText);
    }

    internal static Task SetClipboard(IEnumerable<string> lines)
    {
        return SetClipboard(string.Join(Environment.NewLine, lines));
    }

    internal static Task SetClipboard(IEnumerable<string> lines, string separator)
    {
        return SetClipboard(string.Join(separator, lines));
    }

    internal static Task SetClipboard(string text)
    {
        return ThreadingHelper.RunInSTA(() => Clipboard.SetText(text));
    }
}
