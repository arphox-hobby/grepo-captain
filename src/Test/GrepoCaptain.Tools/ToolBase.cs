using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrepoCaptain.Common;
using GrepoCaptain.Engine;
using GrepoCaptain.InnoDataProvider.Configuration;
using GrepoCaptain.InnoDataProvider.DataProvider;
using GrepoCaptain.InnoDataProvider.Model;

namespace GrepoCaptain.Tools;

public abstract class ToolBase
{
    private readonly Lazy<InnoDataSnapshotProvider> _downloaderLazy = new(CreateSnapshotDownloader);
    private readonly Lazy<Player[]> _playersLazy;
    private readonly Lazy<Town[]> _townsLazy;
    private readonly Lazy<Alliance[]> _alliancesLazy;
    private readonly Lazy<Island[]> _islandsLazy;
    private readonly Lazy<Dictionary<Island, Town[]>> _islandTownsLazy;
    private readonly Lazy<Dictionary<int, Island>> _islandDesignatorsLazy;
    private Dictionary<Island, Town[]> IslandTowns => _islandTownsLazy.Value;
    private Dictionary<int, Island> IslandDesignators => _islandDesignatorsLazy.Value;

    protected InnoDataSnapshotProvider Provider => _downloaderLazy.Value;
    protected Player[] Players => _playersLazy.Value;
    protected Town[] Towns => _townsLazy.Value;
    protected Alliance[] Alliances => _alliancesLazy.Value;
    protected Island[] Islands => _islandsLazy.Value;

    protected ToolBase()
    {
        _playersLazy = new Lazy<Player[]>(LoadPlayers);
        _townsLazy = new Lazy<Town[]>(() => Provider.FetchTowns().Result);
        _alliancesLazy = new Lazy<Alliance[]>(() => Provider.FetchAlliances().Result);
        _islandsLazy = new Lazy<Island[]>(() => Provider.FetchIslands().Result);
        _islandTownsLazy = new Lazy<Dictionary<Island, Town[]>>(LoadIslandTowns);
        _islandDesignatorsLazy = new Lazy<Dictionary<int, Island>>(GetIslandDesignators);
    }

    #region [ Alliance-related ]

    protected Alliance GetAllianceByName(string allianceName) =>
        Alliances.Single(a => a.Name == allianceName);

    protected Player[] GetAllianceMembers(Alliance alliance)
    {
        return Players
            .Where(p => p.AllianceId == alliance.Id)
            .ToArray();
    }

    protected Town[] GetAllianceTowns(Alliance alliance)
    {
        return GetAllianceMembers(alliance)
            .SelectMany(GetPlayerTowns)
            .ToArray();
    }

    protected IEnumerable<Island> GetAllianceIslands(Alliance alliance)
    {
        IEnumerable<Town> oneTownFromEachIsland = GetAllianceTowns(alliance)
            .DistinctBy(t => t.IslandDesignator);

        return oneTownFromEachIsland
            .Select(t => GetIslandByDesignator(t.IslandDesignator))
            .ToArray();
    }

    #endregion

    #region [ Player-related ]

    /// <summary>
    /// Gets the player by id. If not found, throws exception.
    /// </summary>
    protected Player GetPlayer(int? playerId)
    {
        if (playerId == null)
            throw new ArgumentNullException(nameof(playerId));
        return Players.First(p => p.PlayerId == playerId);
    }

    /// <summary>
    /// Gets the player by name. If not found, throws exception.
    /// </summary>
    protected Player GetPlayerExact(string playerName)
    {
        if (string.IsNullOrWhiteSpace(playerName))
            throw new ArgumentException("Value cannot be null or whitespace.", nameof(playerName));
        return Players.Single(p => p.Name == playerName);
    }

    protected Town[] GetPlayerTowns(Player player) =>
        GetPlayerTowns(player.PlayerId);

    protected Town[] GetPlayerTowns(int playerId) =>
        Towns.Where(t => t.PlayerId == playerId).ToArray();

    #endregion

    #region [ Island-related ]

    protected Island GetIsland(int islandId) =>
        Islands.First(i => i.Id == islandId);

    protected Island GetIslandByDesignator(int islandDesignator) =>
        IslandDesignators[islandDesignator];

    protected Town[] GetIslandTowns(Island island)
    {
        IslandTowns.TryGetValue(island, out Town[] value);
        return value ?? Array.Empty<Town>();
    }

    #endregion

    #region [ Town-related ]

    /// <summary>
    /// Gets the town by id. If not found, throws exception.
    /// </summary>
    protected Town GetTown(int townId)
    {
        return Towns.First(t => t.TownId == townId);
    }

    #endregion

    #region [ Favor production ]

    protected static double EstimateFavorProductionForTownCount(
        int townCount,
        bool highPriestess)
    {
        const double numberOfGods = 8.0;
        int citiesPerGodEstimate = (int)Math.Floor(townCount / numberOfGods);
        int totalTempleLevelsPerGodEstimate = citiesPerGodEstimate * 30;

        const double favorProductionBonus = 0; // Let's assume no reward activated
        return FavorProductionCalculator.CalculateFavorProduction(
            totalTempleLevelsPerGodEstimate,
            highPriestess,
            Settings.WorldSpeed,
            favorProductionBonus);
    }

    #endregion

    #region [ Lazy value factories ]

    private static InnoDataSnapshotProvider CreateSnapshotDownloader()
    {
        InnoConfiguration innoConfig = new InnoGamesTomlConfigurationReader().ReadConfiguration();
        HttpClient httpClient = new();
        InnoDataSnapshotDownloader downloader = new(httpClient, innoConfig, Settings.SelectedWorld);
        return new InnoDataSnapshotProvider(downloader);
    }

    private Player[] LoadPlayers()
    {
        return Provider.FetchPlayers().Result.OrderByDescending(p => p.Points).ToArray();
    }

    private Dictionary<Island, Town[]> LoadIslandTowns()
    {
        return Towns
            .GroupBy(t => t.IslandDesignator)
            .ToDictionary(k => IslandDesignators[k.Key], v => v.ToArray());
    }

    private Dictionary<int, Island> GetIslandDesignators()
    {
        return Islands.ToDictionary(k => k.IslandDesignator);
    }

    #endregion
}
