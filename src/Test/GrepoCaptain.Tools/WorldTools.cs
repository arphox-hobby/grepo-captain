﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrepoCaptain.InnoDataProvider.Model;
using Xunit;
using static GrepoCaptain.Tools.Utils.ClipboardHelper;

namespace GrepoCaptain.Tools;

public sealed class WorldTools : ToolBase
{
    [Theory]
    [InlineData(2457)] // Carphi 001
    public async Task Find_ghost_cities_near_selected_town(int townId)
    {
        Town referenceTown = GetTown(townId);

        Town[] towns = Islands
            .SelectMany(island => GetIslandTowns(island))
            .Where(t => t.IsGhostTown)
            .OrderBy(t => t.IslandDistanceTo(referenceTown))
            .ToArray();

        IEnumerable<string> export = towns.Select(t => t.ToBbCode());
        await SetClipboard(export);
    }

}
