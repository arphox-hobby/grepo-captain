using System;

namespace GrepoCaptain.InnoDataProvider.Utils;

internal static class ParseHelper
{
    internal static int? ParseNullableInt(string str)
    {
        return int.TryParse(str, out int v) ? v : null;
    }

    internal static DateTime ParseUnixTimestamp(string str)
    {
        long unixTimestamp = long.Parse(str);
        return DateTimeOffset.FromUnixTimeSeconds(unixTimestamp).UtcDateTime;
    }
}
