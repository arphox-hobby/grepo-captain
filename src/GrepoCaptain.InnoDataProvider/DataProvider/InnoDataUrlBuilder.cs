namespace GrepoCaptain.InnoDataProvider.DataProvider;

internal static class InnoDataUrlBuilder
{
    internal static string Build(string worldIdentifier, string baseUrl, string fileName)
    {
        baseUrl = baseUrl.TrimEnd('/');
        return $"https://{worldIdentifier}.{baseUrl}/{fileName}";
    }
}
