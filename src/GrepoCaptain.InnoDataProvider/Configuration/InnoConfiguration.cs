namespace GrepoCaptain.InnoDataProvider.Configuration;

/// <summary>
/// Model class for storing InnoGames-related information.
/// </summary>
/// <param name="Prefix">The URL prefix for the data query endpoint.</param>
/// <param name="Player">The filename for data of players.</param>
/// <param name="Alliance">The filename for data of alliances.</param>
/// <param name="Town">The filename for data of towns.</param>
/// <param name="Island">The filename for data of islands.</param>
/// <param name="PlayerAll">The filename for data of players' total battle points.</param>
/// <param name="PlayerAtt">The filename for data of players' attack battle points.</param>
/// <param name="PlayerDef">The filename for data of players' defense battle points.</param>
/// <param name="AllianceAll">The filename for data of alliances' total battle points.</param>
/// <param name="AllianceAtt">The filename for data of alliances' attack battle points.</param>
/// <param name="AllianceDef">The filename for data of alliances' defense battle points.</param>
/// <param name="Conquers">The filename for data of town conquers.</param>
/// <param name="StaticUnits">The filename for static data of units.</param>
/// <param name="StaticResearches">The filename for static data of researches.</param>
/// <param name="StaticBuildings">The filename for static data of buildings.</param>
public record InnoConfiguration(
    string Prefix,
    string Player,
    string Alliance,
    string Town,
    string Island,
    string PlayerAll,
    string PlayerAtt,
    string PlayerDef,
    string AllianceAll,
    string AllianceAtt,
    string AllianceDef,
    string Conquers,
    string StaticUnits,
    string StaticResearches,
    string StaticBuildings
);
