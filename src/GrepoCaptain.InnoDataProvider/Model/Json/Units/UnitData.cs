using System.Collections.Generic;
using GrepoCaptain.InnoDataProvider.Model.Json.Enums;
using GrepoCaptain.InnoDataProvider.Model.Json.Serialization;
using Newtonsoft.Json;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Units;

public record UnitData
{
    public string Id { get; init; }
    public string Name { get; init; }
    public string Description { get; init; }
    public int Speed { get; init; }
    public int Attack { get; init; }
    public int Favor { get; init; }
    public int Population { get; init; }
    public string Passive { get; init; }
    public UnitCategory Category { get; init; }
    public Resources Resources { get; init; }
    public int Booty { get; init; }

    /// <summary>
    /// Naval defense value, for non-naval units always 0.
    /// </summary>
    [JsonProperty("defense")]
    [JsonConverter(typeof(NullToDefaultValueConverter))]
    public int DefNaval { get; init; }

    [JsonProperty("max_per_attack")]
    public int? MaxPerAttack { get; init; }

    [JsonProperty("max_per_support")]
    public int? MaxPerSupport { get; init; }

    [JsonProperty("flying")]
    [JsonConverter(typeof(NullToDefaultValueConverter))]
    public bool IsFlying { get; init; }

    [JsonProperty("attack_type")]
    public AttackType AttackType { get; init; }

    [JsonProperty("unit_function")]
    public UnitFunction UnitFunction { get; init; }

    [JsonProperty("is_naval")]
    public bool IsNaval { get; init; }

    [JsonProperty("build_time")]
    public int BuildTime { get; init; }

    [JsonProperty("name_plural")]
    public string NamePlural { get; init; }

    [JsonProperty("def_hack")]
    public int DefHack { get; init; }

    [JsonProperty("def_pierce")]
    public int DefPierce { get; init; }

    [JsonProperty("def_distance")]
    public int DefDistance { get; init; }

    [JsonProperty("god_id")]
    [JsonConverter(typeof(NullToDefaultValueConverter))]
    public God God { get; init; }

    [JsonProperty("building_dependencies")]
    [JsonConverter(typeof(BuildingDependenciesJsonConverter))]
    public Dictionary<string, int> BuildingDependencies { get; init; }

    [JsonProperty("research_dependencies")]
    [JsonConverter(typeof(ResearchDependenciesJsonConverter))]
    public string[] ResearchDependencies { get; init; }

    [JsonProperty("special_abilities")]
    public string[] SpecialAbilities { get; init; }

    /// <remarks>
    /// This is almost useless because true only for sword, slinger, archer and hoplite.
    /// </remarks>
    [JsonProperty("infantry")]
    [JsonConverter(typeof(NullToDefaultValueConverter))]
    public bool IsInfantry { get; init; }

    [JsonProperty("is_npc_unit_only")]
    public bool IsNpcUnitOnly { get; init; }

    /// <summary>
    /// Calculates DefHack + DefPierce + DefDistance + NavalDefense
    /// </summary>
    public int TotalDefense => DefHack + DefPierce + DefDistance + DefNaval;

    /// <summary>
    /// Tells whether the unit is a mythological unit.
    /// </summary>
    public bool IsMythological => God != God.None;
}
