using System;
using System.Collections.Generic;
using GrepoCaptain.InnoDataProvider.Model.Json.Serialization;
using Newtonsoft.Json;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Buildings;

public record BuildingData
{
    // other properties not processed yet:
    // resourcesForLevelFactor

    public string Id { get; init; }
    public string Name { get; init; }
    public string Controller { get; init; }
    public string Description { get; init; }
    public string Coordinates { get; init; }
    public Resources Resources { get; init; }
    public int Points { get; init; }

    [JsonProperty("wood_factor")]
    public double WoodFactor { get; init; }

    [JsonProperty("stone_factor")]
    public double StoneFactor { get; init; }

    [JsonProperty("iron_factor")]
    public double IronFactor { get; init; }

    [JsonConverter(typeof(NullToDefaultValueConverter))]
    [JsonProperty("hide_factor")]
    public int HideFactor { get; init; }

    [JsonProperty("min_level")]
    public int MinLevel { get; init; }

    [JsonProperty("max_level")]
    public int MaxLevel { get; init; }

    [JsonProperty("pop")]
    public int Population { get; init; }

    [JsonProperty("pop_factor")]
    public double PopulationFactor { get; init; }

    [JsonProperty("points_factor")]
    public double PointsFactor { get; init; }

    [JsonProperty("build_time")]
    public int BuildTime { get; init; }

    [JsonProperty("build_time_factor")]
    public double BuildTimeFactor { get; init; }

    [JsonProperty("build_time_reduction")]
    public double BuildTimeReduction { get; init; }

    [JsonProperty("bolt_protected")]
    [JsonConverter(typeof(NullToDefaultValueConverter))]
    public bool IsBoltProtected { get; init; }

    [JsonProperty("special")]
    public bool IsSpecial { get; init; }

    [JsonConverter(typeof(BuildingDependenciesJsonConverter))]
    public Dictionary<string, int> Dependencies { get; init; }

    [JsonProperty("fixed_building_times")]
    [JsonConverter(typeof(FixedBuildingTimesJsonConverter))]
    public Dictionary<int, int> FixedBuildingTimes { get; init; }

    [JsonProperty("level_build_time_factors")]
    [JsonConverter(typeof(LevelBuildTimeFactorsJsonConverter))]
    public Dictionary<int, BuildTimeFactor> LevelBuildTimeFactors { get; init; }

    [JsonProperty("image_levels")]
    public int[] ImageLevels { get; init; }

    [JsonProperty("offset_value_map")]
    public int[] OffsetValueMap { get; init; }

    /// <summary>
    /// Returns the total population value at the given <paramref name="level"/>,
    /// rounded to one (tenth) places.
    /// </summary>
    public double CalculateTotalPopulationAtLevel(int level)
    {
        CheckLevel(level);
        double pop = Population * Math.Pow(level, PopulationFactor);
        return Math.Round(pop, 1);
    }

    /// <summary>
    /// Returns the population value necessary to build the building to level <paramref name="level"/>,
    /// rounded to one (tenth) places.
    /// </summary>
    public double CalculatePopulationForLevel(int level)
    {
        CheckLevel(level);
        double popCurrent = CalculateTotalPopulationAtLevel(level);
        double popPrevious = level == 1 ? 0 : CalculateTotalPopulationAtLevel(level - 1);
        double forLevel = popCurrent - popPrevious;
        return Math.Round(forLevel, 1);
    }

    private void CheckLevel(int level)
    {
        if (level < MinLevel)
            throw new ArgumentOutOfRangeException(nameof(level), level,
                $"Value cannot be less than {nameof(MinLevel)}");

        if (level > MaxLevel)
            throw new ArgumentOutOfRangeException(nameof(level), level,
                $"Value cannot be greater than {nameof(MaxLevel)}");
    }
}
