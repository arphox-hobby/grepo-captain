namespace GrepoCaptain.InnoDataProvider.Model.Json.Enums;

public enum God
{
    None = 0,
    All = 1,
    Zeus = 2,
    Poseidon = 3,
    Hera = 4,
    Athena = 5,
    Hades = 6,
    Artemis = 7,
    Aphrodite = 8,
    Ares = 9,
}
