using System.Runtime.Serialization;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Enums;

public enum UnitFunction
{
    [EnumMember(Value = "function_none")]
    None = 0,

    [EnumMember(Value = "function_def")]
    Defensive = 1,

    [EnumMember(Value = "function_off")]
    Offensive = 2,

    [EnumMember(Value = "function_both")]
    Both = 3,
}
