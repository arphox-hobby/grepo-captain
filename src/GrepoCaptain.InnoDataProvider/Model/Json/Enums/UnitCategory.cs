using System;
using System.Runtime.Serialization;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Enums;

[Flags]
public enum UnitCategory
{
    Regular = 1,
    Mythological = 2,
    Ground = 4,
    Naval = 8,

    [EnumMember(Value = "regular_ground")]
    RegularGround = Regular | Ground,

    [EnumMember(Value = "mythological_ground")]
    MythologicalGround = Mythological | Ground,

    [EnumMember(Value = "regular_naval")]
    RegularNaval = Regular | Naval,

    [EnumMember(Value = "mythological_naval")]
    MythologicalNaval = Mythological | Naval,
}
