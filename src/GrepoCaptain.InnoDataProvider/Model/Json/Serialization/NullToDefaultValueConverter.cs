using System;
using Newtonsoft.Json;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Serialization;

public class NullToDefaultValueConverter : JsonConverter
{
    public override bool CanWrite => false;

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }

    public override object ReadJson(
        JsonReader reader,
        Type objectType,
        object existingValue,
        JsonSerializer serializer)
    {
        object value = reader.Value;
        if (reader.TokenType == JsonToken.Null || value == null)
            return objectType.IsValueType ? Activator.CreateInstance(objectType) : null;
        if (value.GetType() == objectType)
            return reader.Value;
        if (value is long lng && objectType == typeof(int))
            return checked((int)lng);
        if (objectType.IsEnum)
            return Enum.Parse(objectType, (string)value, true);

        throw new NotImplementedException();
    }

    public override bool CanConvert(Type objectType)
    {
        throw new NotImplementedException();
    }
}
