using System;
using System.Collections.Generic;
using GrepoCaptain.InnoDataProvider.Model.Json.Buildings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Serialization;

public sealed class LevelBuildTimeFactorsJsonConverter : JsonConverter
{
    public override bool CanWrite => false;

    public override object ReadJson(
        JsonReader reader,
        Type objectType,
        object existingValue,
        JsonSerializer serializer)
    {
        // Yeah, well done InnoGames:
        // - when there are no dependencies, it's null or an empty array "[ ]"
        // - but when it does, it's an object "{ ... }" ...

        if (reader.TokenType == JsonToken.Null)
            return new Dictionary<int, BuildTimeFactor>();

        if (reader.TokenType == JsonToken.StartArray)
        {
            if (JArray.Load(reader).Count == 0)
                return new Dictionary<int, BuildTimeFactor>();

            throw new Exception("Unexpected schema, review code");
        }

        if (reader.TokenType == JsonToken.StartObject)
            return JObject.Load(reader).ToObject<Dictionary<int, BuildTimeFactor>>();

        throw new NotImplementedException("Unexpected schema, review code");
    }

    public override bool CanConvert(Type objectType)
    {
        throw new NotImplementedException();
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }
}
