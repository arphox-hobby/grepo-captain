using System;
using GrepoCaptain.InnoDataProvider.Utils;

namespace GrepoCaptain.InnoDataProvider.Model;

public record Conquer
{
    public int TownId { get; private init; }
    public DateTime Timestamp { get; private init; }
    public int NewPlayerId { get; private init; }
    public int? OldPlayerId { get; private init; }
    public int? NewAllianceId { get; private init; }
    public int? OldAllianceId { get; private init; }
    public int TownPoints { get; private init; }

    internal static Conquer ParseConquer(string row)
    {
        // Example rows:
        // 4917,1661912645,848919096,848913699,78,348,9249
        // 6288,1661885437,902950,,,,499

        string[] parts = row.Split(',');

        int townId = int.Parse(parts[0]);
        DateTime timestamp = ParseHelper.ParseUnixTimestamp(parts[1]);
        int newPlayerId = int.Parse(parts[2]);
        int? oldPlayerId = ParseHelper.ParseNullableInt(parts[3]);
        int? newAllianceId = ParseHelper.ParseNullableInt(parts[4]);
        int? oldAllianceId = ParseHelper.ParseNullableInt(parts[5]);
        int townPoints = int.Parse(parts[6]);

        return new Conquer
        {
            TownId = townId,
            Timestamp = timestamp,
            NewPlayerId = newPlayerId,
            OldPlayerId = oldPlayerId,
            NewAllianceId = newAllianceId,
            OldAllianceId = oldAllianceId,
            TownPoints = townPoints,
        };
    }
}
