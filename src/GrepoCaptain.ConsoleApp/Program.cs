﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrepoCaptain.Common;
using GrepoCaptain.InnoDataProvider.Configuration;
using GrepoCaptain.InnoDataProvider.DataProvider;
using GrepoCaptain.InnoDataProvider.Model;
using GrepoCaptain.InnoDataProvider.Model.Json.Buildings;
using GrepoCaptain.InnoDataProvider.Model.Json.Enums;
using GrepoCaptain.InnoDataProvider.Model.Json.Units;
using MoreLinq.Extensions;
using HuWorlds = GrepoCaptain.InnoDataProvider.Configuration.Constants.HungarianWorldIdentifiers;

namespace GrepoCaptain.ConsoleApp;

public static class Program
{
    public static async Task Main()
    {
        return;

        InnoConfiguration innoConfig = new InnoGamesTomlConfigurationReader().ReadConfiguration();
        HttpClient httpClient = new();
        InnoDataSnapshotDownloader downloader = new(httpClient, innoConfig, HuWorlds.Carphi);
        InnoDataSnapshotProvider provider = new(downloader);

        Player[] players = await provider.FetchPlayers();
        Town[] towns = await provider.FetchTowns();
        Island[] islands = await provider.FetchIslands();
        Dictionary<int, Island> islandDesignators = islands.ToDictionary(k => k.IslandDesignator);
        Dictionary<Island, Town[]> islandTowns = towns
            .GroupBy(t => t.IslandDesignator)
            .ToDictionary(k => islandDesignators[k.Key], v => v.ToArray());
        Alliance[] alliances = await provider.FetchAlliances();
        Conquer[] conquers = await provider.FetchConquers();

        // await ProcessBuildings(provider);
        // await ProcessUnits(provider);
        // await FindGhostTownsNearby(provider);
        // BattleCalculator.Run();
    }

    private static async Task ProcessBuildings(InnoDataSnapshotProvider provider)
    {
        BuildingData[] buildings = await provider.FetchStaticBuildings();

        StringBuilder sb = new();

        await ThreadingHelper.RunInSTA(() => Clipboard.SetText(sb.ToString()));
        Console.WriteLine("Done");
    }

    private static async Task ProcessUnits(InnoDataSnapshotProvider provider)
    {
        UnitData[] units = (await provider.FetchStaticUnits())
            .Where(u => u.Id != "militia")
            .ToArray();

        StringBuilder sb = new();

        foreach (UnitData unit in units)
        {
            double pop = unit.Population;
            List<string> values = new()
            {
                unit.Name,
                AttackTypeToIcon(unit),
                unit.Attack.ToString(),
                unit.DefHack.ToString(),
                unit.DefPierce.ToString(),
                unit.DefDistance.ToString(),
                unit.DefNaval.ToString(),
                unit.Speed.ToString(),
                unit.Booty.ToString(),
                unit.Resources.Wood.ToString(),
                unit.Resources.Stone.ToString(),
                unit.Resources.Iron.ToString(),
                unit.Favor.ToString(),
                unit.Population.ToString(),
                unit.God.ToString(),
                unit.TotalDefense.ToString(),
                unit.Resources.Total.ToString(),
                // PER POPULATION
                (unit.Attack / pop).ToString("F1"),
                (unit.TotalDefense / pop).ToString("F1"),
                ((unit.Attack + unit.TotalDefense) / pop).ToString("F1"),
                (unit.Resources.Total / pop).ToString("F1"),
                (unit.DefHack / pop).ToString("F1"),
                (unit.DefPierce / pop).ToString("F1"),
                (unit.DefDistance / pop).ToString("F1"),
                (unit.DefNaval / pop).ToString("F1"),
            };

            sb.AppendLine(string.Join('\t', values));
        }

        await ThreadingHelper.RunInSTA(() => Clipboard.SetText(sb.ToString()));
        Console.WriteLine("Done");
    }

    private static string AttackTypeToIcon(UnitData unit)
    {
        const string hackIcon = "https://wiki.en.grepolis.com/images/1/15/Att_hack.png";
        const string pierceIcon = "https://wiki.en.grepolis.com/images/6/64/Att_sharp.png";
        const string distanceIcon = @"https://wiki.en.grepolis.com/images/f/f2/Attackdist.png";
        const string navalIcon = @"https://wiki.en.grepolis.com/images/d/d5/Attboat.png";
        const int imageSize = 20;
        string template = $@"=IMAGE(""{{0}}"";4;{imageSize};{imageSize})";

        return unit.AttackType switch
        {
            AttackType.Hack => string.Format(template, hackIcon),
            AttackType.Pierce => string.Format(template, pierceIcon),
            AttackType.Distance => string.Format(template, distanceIcon),
            AttackType.Naval => string.Format(template, navalIcon),
            _ => throw new ArgumentOutOfRangeException(),
        };
    }

    private static async Task FindGhostTownsNearby(InnoDataSnapshotProvider provider)
    {
        Town[] towns = await provider.FetchTowns();
        Town myHydraTown = towns.Single(t => t.TownId == 6933);
        towns
            .Where(t => t.IsGhostTown)
            .Select(t => new
            {
                Dist = t.IslandDistanceTo(myHydraTown),
                t.Name,
                t.Points,
                BB = t.ToBbCode(),
            })
            .OrderBy(t => t.Dist)
            .ForEach(Console.WriteLine);
    }
}
