using System;
using System.Collections.Generic;

namespace GrepoCaptain.BBCoder;

public sealed class TableBuilder
{
    private readonly List<string> _rowBBs = new();
    public BBCode HeaderBB { get; private set; }
    public IReadOnlyList<string> RowBBs => _rowBBs;

    public TableBuilder SetHeader(IEnumerable<string> items)
    {
        BBCode headerContent = string.Join("[||]", items);
        HeaderBB = headerContent.WrapWithTag("**");
        return this;
    }

    public TableBuilder SetHeader(params string[] items) =>
        SetHeader((IEnumerable<string>)items);

    public TableBuilder AddRow(IEnumerable<string> items)
    {
        BBCode rowContent = string.Join("[|]", items);
        rowContent = rowContent.WrapWithTag("*");
        _rowBBs.Add(rowContent);
        return this;
    }

    public TableBuilder AddRow(params string[] items) =>
        AddRow((IEnumerable<string>)items);

    public string Build(string lineSeparator = null)
    {
        lineSeparator ??= Environment.NewLine;
        List<string> items = new()
        {
            "[table]",
            HeaderBB,
        };
        items.AddRange(_rowBBs);
        items.Add("[/table]");

        return string.Join(lineSeparator, items);
    }
}
