﻿using System;

namespace GrepoCaptain.Engine;

/// <summary>
/// Just some temporary stuff, need to implement properly later.
/// </summary>
public static class FavorProductionCalculator
{
    /// <summary>
    /// Calculates favor production speed
    /// </summary>
    /// <param name="totalTempleLevels">The sum of temple levels for the selected god. Add +5 for each Divine Statue, too.</param>
    /// <param name="highPriestess">Whether the high priestess premium is active.</param>
    /// <param name="worldSpeed">The world speed, e.g. 4.</param>
    /// <param name="extraFavorProductionFactor">Add any % favor production bonus here as a factor (so e.g. 50% becomes 0.5)</param>
    /// <returns>The hourly favor production.</returns>
    public static double CalculateFavorProduction(
        int totalTempleLevels,
        bool highPriestess,
        int worldSpeed,
        double extraFavorProductionFactor = 0)
    {
        double highPriestessBonusFactor = highPriestess ? 0.5 : 0;
        double templeLevelSqrt = Math.Sqrt(totalTempleLevels);
        double totalBonusFactor = extraFavorProductionFactor + highPriestessBonusFactor;

        double favorProduction =
            worldSpeed *
            templeLevelSqrt *
            (1 + totalBonusFactor);

        return Math.Round(favorProduction, 1);
    }
}
