# GrepoCaptain

A chrome-ot lehet extra paraméterekkel indítani.  
Pl. ami letiltja az "elaltatását" a javascript függvényeknek amin a játék alapul.  

Setup:
1. Kimész az asztalra
2. Jobb klikk, új -> Parancsikon
3. bemásolod ezt az elem helyére amit kér:

`
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-background-timer-throttling --disable-backgrounding-occluded-windows --disable-renderer-backgrounding
`

aztán továbbmész, megadod névnek amit akarsz, pl. "grepolis chrome"

Egyéb infok: https://github.com/GoogleChrome/chrome-launcher/blob/main/docs/chrome-flags-for-tools.md
